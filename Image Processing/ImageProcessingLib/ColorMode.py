from ImageProcessingLib import *

@dataclass
class colorMode:
    img_path: str = ''
    extension: list = field(default_factory=lambda: ['.jpg', '.jpeg', '.png', '.bmp'])

    def __post_init__(self):
        if os.path.isfile(self.img_path) and os.path.splitext(self.img_path)[1].lower() in self.extension:
            self.load_img()
        else:
            pass

    def load_img(self):
        self.im_bgr = cv2.imread(self.img_path)
        self.im_rgb = self.im_bgr[:, :, ::-1]
        self.h, self.w, self.c = self.im_bgr.shape
        self.r = self.im_rgb[:, :, 0]
        self.g = self.im_rgb[:, :, 1]
        self.b = self.im_rgb[:, :, 2]


    @property
    def filename(self) -> str:
        return self.img_path
    
    @filename.setter
    def filename(self, value):
        self.img_path = value

    @property
    def rgb(self) -> np.ndarray:
        return self.r, self.g, self. b

    @rgb.setter
    def rgb(self, matrix):
        self.r = matrix[:, :, 0]
        self.g = matrix[:, :, 1]
        self.b = matrix[:, :, 2]

    def gray(self) -> np.ndarray:
        mat = np.array([[0.299], [0.587], [0.114]])
        return np.dot(self.im_rgb, mat).astype(np.uint8)

    def rgb2yuv(self) -> np.ndarray:
        mat = np.array([[0.299, -0.169,  0.500],
                        [0.587, -0.331, -0.419],
                        [0.114,  0.500, -0.081]])
        shift_mat = np.array([0, 128, 128])
        yuv = (np.dot(self.im_rgb, mat)+shift_mat).astype(np.uint8)
        return yuv[:, :, 0], yuv[:, :, 1], yuv[:, :, 2]

    def yuv2rgb(self, y, u, v) -> np.ndarray:
        yuv = np.zeros((self.h, self.w, self.c))
        for row, row_mat in enumerate(zip(y, u, v)):
            for col, (element_y, element_u, element_v) in enumerate(zip(row_mat[0], row_mat[1], row_mat[2])):
                yuv[row][col] = [element_y, element_u, element_v]

        mat = np.array([[ 1.000,  1.000, 1.000],
                        [-0.000, -0.343, 1.772],
                        [ 1.402, -0.714, 0.000]])
        shift_mat = np.array([0, -128, -128])
        return (np.dot(yuv+shift_mat, mat)).astype(np.uint8)

    def rgb2hsl(self) -> np.ndarray:
        r = (self.r/255).reshape(-1)
        g = (self.g/255).reshape(-1)
        b = (self.b/255).reshape(-1)
        max_ = np.maximum(r, np.maximum(g, b))
        min_ = np.minimum(r, np.minimum(g, b))
        delta = max_ - min_

        h = np.zeros(self.h*self.w)
        s = np.zeros(self.h*self.w)
        l = (max_+min_) / 2
        for idx, (max_v, _, delta_v, r_v, g_v, b_v) in enumerate(zip(max_, min_, delta, r, g, b)):
            if delta_v == 0:
                h[idx] = 0
            elif max_v == r_v:
                h[idx] = 60*(((g_v-b_v)/delta_v) % 6)
            elif max_v == g_v:
                h[idx] = 60*(((b_v-r_v)/delta_v) + 2)
            elif max_v == b_v:
                h[idx] = 60*(((r_v-g_v)/delta_v) + 4)
            
            if delta_v == 0:
                s[idx] = 0
            else:
                s[idx] = delta_v/(1-abs(2*l[idx]-1))

        h = h.reshape((self.h, self.w)).astype(np.uint8)
        s = np.round(s.reshape((self.h, self.w)), decimals=3)
        l = np.round(l.reshape((self.h, self.w)), decimals=3)
        return h, s, l

    def hsl2rgb(self, h, s, l):
        h = h.reshape(-1)
        s = s.reshape(-1)
        l = l.reshape(-1)

        r = np.zeros(self.h*self.w)
        g = np.zeros(self.h*self.w)
        b = np.zeros(self.h*self.w)
        for idx, (h_v, s_v, l_v) in enumerate(zip(h, s, l)):
            if s_v == 0:
                r[idx] = g[idx] = b[idx] = l_v
            else:
                if l_v >= 1/2:
                    q = l_v+s_v-(l_v*s_v)
                else:
                    q = l_v*(1+s_v)
                p = 2*l_v-q
                h_norm = h_v/360
                th_r = h_norm+1/3+1 if h_norm+1/3 < 0 else h_norm+1/3-1 if h_norm+1/3 > 1 else h_norm+1/3
                th_g = h_norm+1 if h_norm < 0 else h_norm-1 if h_norm > 1 else h_norm
                th_b = h_norm-1/3+1 if h_norm-1/3 < 0 else h_norm-1/3-1 if h_norm-1/3 > 1 else h_norm-1/3

                for th, output in zip([th_r, th_g, th_b], [r, g, b]):
                    if th < 1/6:
                        output[idx] = p+((q-p)*6*th)
                    elif 1/6 <= th < 1/2:
                        output[idx] = q
                    elif 1/2 <= th < 2/3:
                        output[idx] = p+((q-p)*6*(2/3-th))
                    else:
                        output[idx] = p

        r = (r*255).reshape((self.h, self.w)).astype(np.uint8)
        g = (g*255).reshape((self.h, self.w)).astype(np.uint8)
        b = (b*255).reshape((self.h, self.w)).astype(np.uint8)
        return np.array([r, g, b])


    def rgb2hsv(self) -> np.ndarray:
        r = (self.r/255).reshape(-1)
        g = (self.g/255).reshape(-1)
        b = (self.b/255).reshape(-1)
        max_ = np.maximum(r, np.maximum(g, b))
        min_ = np.minimum(r, np.minimum(g, b))
        delta = max_ - min_

        h = np.zeros(self.h*self.w)
        s = np.zeros(self.h*self.w)
        v = max_

        for idx, (max_v, min_v, delta_v, r_v, g_v, b_v) in enumerate(zip(max_, min_, delta, r, g, b)):
            if delta_v == 0:
                h[idx] = 0
            elif max_v == r_v:
                h[idx] = 60*(((g_v-b_v)/delta_v) % 6)
            elif max_v == g_v:
                h[idx] = 60*(((b_v-r_v)/delta_v) + 2)
            elif max_v == b_v:
                h[idx] = 60*(((r_v-g_v)/delta_v) + 4)
            
            if delta_v == 0:
                s[idx] = 0
            else:
                s[idx] = delta_v/max_v

        h = h.reshape((self.h, self.w)).astype(np.uint8)
        s = np.round(s.reshape((self.h, self.w)), decimals=2)
        v = np.round(v.reshape((self.h, self.w)), decimals=2)
        return h, s, v

    def hsv2rgb(self, h, s, v) -> np.ndarray:
        h = h.reshape(-1)
        s = s.reshape(-1)
        v = v.reshape(-1)

        r = np.zeros(self.h*self.w)
        g = np.zeros(self.h*self.w)
        b = np.zeros(self.h*self.w)

        for idx, (h_v, s_v, v_v) in enumerate(zip(h, s, v)):
            h_i = math.floor(h_v/60)
            f = h_v/60-h_i
            p = v_v*(1-s_v)
            q = v_v*(1-f*s_v)
            t = v_v*(1-(1-f)*s_v)

            if h_i == 0:
                r[idx], g[idx], b[idx]= v_v, t, p
            elif h_i == 1:
                r[idx], g[idx], b[idx]= q, v_v, p
            elif h_i == 2:
                r[idx], g[idx], b[idx]= p, v_v, t
            elif h_i == 3:
                r[idx], g[idx], b[idx]= p, q, v_v
            elif h_i == 4:
                r[idx], g[idx], b[idx]= t, p, v_v
            elif h_i == 5:
                r[idx], g[idx], b[idx]= v_v, p, q

        r = (r*255).reshape((self.h, self.w)).astype(np.uint8)
        g = (g*255).reshape((self.h, self.w)).astype(np.uint8)
        b = (b*255).reshape((self.h, self.w)).astype(np.uint8)
        return np.array([r, g, b])

    def rgb2ycbcr(self) -> np.ndarray:
        mat = np.array([[0.257, -0.148,  0.439],
                        [0.504, -0.291, -0.368],
                        [0.098,  0.439, -0.071]])
        shift_mat = np.array([16, 128, 128])
        ycbcr = (np.dot(self.im_rgb, mat)+shift_mat).astype(np.uint8)
        return ycbcr[:, :, 0], ycbcr[:, :, 1], ycbcr[:, :, 2]
    
    def ycbcr2rgb(self, y, cb, cr) -> np.ndarray:
        ycbcr = np.zeros((self.h, self.w, self.c))
        for row, row_mat in enumerate(zip(y, cb, cr)):
            for col, (element_y, element_cb, element_cr) in enumerate(zip(row_mat[0], row_mat[1], row_mat[2])):
                ycbcr[row][col] = [element_y, element_cb, element_cr]

        mat = np.array([[1.164,  1.164, 1.164],
                        [0.000, -0.391, 2.018],
                        [1.596, -0.813, 0.000]])
        shift_mat = np.array([-222.912, 135.488, -276.928])
        return (np.dot(ycbcr, mat)+shift_mat).astype(np.uint8)
