import os
import sys
import cv2
import random
import numpy as np
from sklearn.cluster import KMeans

if __name__ == '__main__':
    im = cv2.imread('./lion.jpg', 0)
    height, width = im.shape
    
    kmeans = KMeans(n_clusters=3)
    kmeans.fit(im.reshape(height*width, 1))
    threshold = sorted(map(lambda val: int(val[0]), kmeans.cluster_centers_))

    canvas = np.zeros((3 * height, 3 * width, 3), np.uint8)
    canvas.fill(255)
    for h in range(0, height*3, 3):
        for w in range(0, width*3, 3):
            val = im[int(h/3)][int(w/3)]
            if val <= threshold[0]:
                cv2.putText(canvas, str(random.randint(0,9)), (w, h), cv2.FONT_HERSHEY_PLAIN, 0.40, 1)
            elif val <= threshold[1]:
                cv2.putText(canvas, '-', (w, h), cv2.FONT_HERSHEY_PLAIN, 0.40, 1)
            elif val <= threshold[2]:
                cv2.putText(canvas, '.', (w, h), cv2.FONT_HERSHEY_PLAIN, 0.40, 1)
            else:
                cv2.putText(canvas, ' ', (w, h), cv2.FONT_HERSHEY_PLAIN, 0.40, 1)

    cv2.imwrite('./output.png', canvas)