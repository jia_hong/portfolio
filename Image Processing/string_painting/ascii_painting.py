import cv2
import numpy as np

ascii = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1{}[]?-_+~<>i!lI;:,\"^`'. "

def get_char(gray_val):
    unit = 256.0/len(ascii)
    return ascii[int(gray_val/unit)]
    
if __name__ == '__main__':
    im = cv2.imread('./lion.jpg', 0)
    height, width = im.shape

    canvas = np.zeros((3*height, 3*width, 3), np.uint8)
    canvas.fill(255)

    for h in range(0, height*3, 3):
        for w in range(0, width*3, 3):
            val = im[int(h/3)][int(w/3)]
            cv2.putText(canvas, get_char(val), (w, h), cv2.FONT_HERSHEY_PLAIN, 0.4, 1)
    cv2.imwrite('./output.png', canvas)