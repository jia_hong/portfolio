import os
import sys
import cv2
import numpy as np
from sklearn.cluster import KMeans

if __name__ == '__main__':
    im = cv2.imread('./image.jpg')
    kmeans = KMeans(n_clusters=5)
    kmeans.fit(im.reshape(-1, 3))
    colors = kmeans.cluster_centers_
    
    canvas_height = 200
    canvas_width = 500
    canvas = np.zeros((canvas_height, canvas_width, 3), dtype = 'uint8')
    for idx, color in enumerate(colors):
        cv2.rectangle(canvas, (idx*100, 0), (idx*100+99, canvas_height), list(map(lambda val: int(val), color)), -1)
    cv2.imwrite('color_palette.png', canvas)