import re
import os
import csv
import sys
import time
import requests

from bs4 import BeautifulSoup
from dataclasses import dataclass, field

@dataclass
class buildingCrawler:
    html: str = 'https://www.888civil.com/vendor'

    def __post_init__(self):
        self.get_response(self.html)

    def get_response(self, href):
        self.href = href
        self.response = requests.get(href)
        self.soup = BeautifulSoup(self.response.text, 'html.parser')

    @property
    def industries_href(self):
        links = list()
        industries = self.soup.find('div', id='ttr_sidebar_left_margin').find_all('div', class_='categoryGroup LEVEL2')
        for industry in industries:
            href = list((a.get('title'), a['href']) for a in industry.select('a[href]'))
            for name, link in href:
                links.append((name, link))
        return links
    
    @property
    def sub_industries_href(self):
        links = list()
        industries = self.soup.find('div', class_='m2l') \
                            .find('div', class_='sort') \
                            .find_all('a')
        for industry in industries:
            href = industry['href']
            links.append((industry.get_text(), href))
        return links
    
    @property
    def categories(self):
        links = list()
        industries = self.soup.find('div', class_='m2l') \
                            .find('div', class_='sort') \
                            .find_all('a')
        for industry in industries:
            href = industry['href']
            links.append((industry.get_text(), href))
        return links
    
    @property
    def pages(self):
        res = self.soup.find('div', class_='pageCount')
        val = res.select_one('div').get_text()
        return val.split(' ')[5]

    def get_content(self):
        company = list()
        res = self.soup.find_all('div', class_='mainbar')
        for data in res:
            company_dict = dict()
            if data.find('div', class_='vendorData-logo') is not None:
                company_dict['VIP'] = 1
            else:
                company_dict['VIP'] = 0    
            company_dict['公司名稱'] = data.find('div', class_='vendorData-title').get_text()
            company_dict['聯絡電話'] = "'"+data.find('div', class_='vendorData-tel').get_text().replace(' ', '')
            address = data.find('div', class_='vendorData-address').get_text()
            try:
                company_dict['所在地'] = address.split(' ')[2]
                company_dict['地址'] = address.split(' ')[3]
            except:
                company_dict['所在地'] = ''
                company_dict['地址'] = ''
            company_dict['主營行業'] = data.find('div', class_='vendorDetail-bar-detail').get_text()
            company.append(company_dict)
        return company

if __name__ == '__main__':
    crawler = buildingCrawler()
    industries = crawler.industries_href
    for industry, href in industries:
        crawler.get_response(href)
        pages = crawler.pages
        for page in range(1, int(pages)+1):
            print('{} -> {}'.format(industry, page))
            crawler.get_response(href+'?page={}&onePage=10'.format(page))
            for data in crawler.get_content():
                data['產業類別'] = industry
                output = dict(sorted(data.items(), key=lambda item: item[0]))
                try:
                    with open('./building_888.csv', 'a') as f:
                        fieldnames = ['產業類別', 'VIP', '公司名稱', '聯絡電話', '所在地', '地址', '主營行業']
                        w = csv.DictWriter(f, fieldnames=fieldnames)
                        w.writerow(output)
                except:
                    pass