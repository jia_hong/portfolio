import re
import os
import csv
import sys
import time
import requests

from bs4 import BeautifulSoup
from dataclasses import dataclass, field

from selenium import webdriver
import selenium.webdriver.support.ui as ui
from selenium.webdriver.chrome.options import Options

@dataclass
class buildingCrawler:
    html: str = ''

    @property
    def website(self) -> str:
        return self.html
    
    @website.setter
    def website(self, value):
        self.html = value

    def get_response(self, href):
        self.href = href
        self.response = requests.get(href)
        self.soup = BeautifulSoup(self.response.text, 'html.parser')

    @property
    def main_industries_href(self):
        links = list()
        industries = self.soup.find_all('a', {'class': ['product-link', 'product-link2']})
        for industry in industries:
            links.append((industry.get_text().replace('\xa0', ''), 
                            '/'.join(self.href.split('/')[:-1])+'/{}'.format(industry['href'])))
        return links
    
    @property
    def sub_industries_href(self):
        links = list()
        industries = self.soup.find_all('a', {'class': ['product-link']})
        for industry in industries:
            links.append((industry.get_text().replace('\xa0', ''), 
                            '/'.join(self.href.split('/')[:-1])+'/{}'.format(industry['href'])))
        return links
    
    @property
    def pages(self):
        res = self.soup.find_all('span', class_='page')
        return re.findall(r'\d+', res[-1].get_text())[0]

    def get_content(self):
        company = list()
        res = self.soup.find_all('a', class_='company-word3')
        for data in res:
            wait = ui.WebDriverWait(chrome, 10)
            chrome.get(self.href)
            wait.until(lambda chrome: chrome.find_element_by_link_text(data.get_text()))
            chrome.find_element_by_link_text(data.get_text()).click()
            cmp_name, href = data.get_text(), chrome.current_url
            print(cmp_name, href)
            if self.get_detail(cmp_name, href) is not None:
                company.append(self.get_detail(cmp_name, href))
        return company

    def get_detail(self, name, href):
        company_dict = dict()
        company_dict['公司名稱'] = name
        response = requests.get(href)
        soup = BeautifulSoup(response.text, 'html.parser')
        filter_ = ['公司電話', '公司地址', '主要產品']
        for k, v in zip(soup.find_all('td', class_='list_td2'), soup.find_all('td', class_='list_td')[1:]):
            if any(filter in k.get_text() for filter in filter_):
                company_dict[k.get_text().replace(':', '')] = v.get_text()
        return company_dict

if __name__ == '__main__':
    global chrome

    options = Options()
    options.add_argument("--disable-notifications")
    chrome = webdriver.Chrome('./chromedriver', chrome_options=options)
    
    crawler = buildingCrawler()
    for idx in range(1, 2):
        website = 'http://www.tami.org.tw/category/product-new1.php?on={}'.format(idx)
        crawler.get_response(website)
        main_industries = crawler.main_industries_href
        for main_industry, main_href in main_industries:
            crawler.get_response(main_href)
            sub_industries = crawler.sub_industries_href
            for sub_industry, sub_href in sub_industries:
                print('{} -> {}'.format(main_industry, sub_industry))
                crawler.get_response(sub_href)
                pages = crawler.pages
                for page in range(1, int(pages)+1):
                    crawler.get_response(sub_href+'&on={}'.format(page))
                    crawler.get_content()
                    for data in crawler.get_content():
                        data['主產業類別'] = main_industry
                        data['子產業類別'] = sub_industry
                        output = dict(sorted(data.items(), key=lambda item: item[0]))
                        try:
                            with open('./building.csv', 'a') as f:
                                fieldnames = ['主產業類別', '子產業類別', '公司名稱', '公司電話', '公司地址', '主要產品']
                                w = csv.DictWriter(f, fieldnames=fieldnames)
                                w.writerow(output)
                        except:
                            pass
