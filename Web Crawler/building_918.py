import re
import os
import csv
import sys
import time
import requests

from bs4 import BeautifulSoup
from dataclasses import dataclass, field

@dataclass
class buildingCrawler:
    html: str = 'https://918bms.com'

    def __post_init__(self):
        self.get_response(self.html)

    def get_response(self, href):
        self.href = href
        self.response = requests.get(href)
        self.soup = BeautifulSoup(self.response.text, 'html.parser')

    @property
    def main_industries_href(self):
        links = list()
        industries = self.soup.find('li', class_='dropdown nobackground 4').find_all('li')
        for industry in industries:
            href = list(a['href'] for a in industry.select('a[href]'))[0]
            links.append((industry.get_text(), href))
        return links
    
    @property
    def sub_industries_href(self):
        links = list()
        industries = self.soup.find('div', class_='m2l') \
                            .find('div', class_='sort') \
                            .find_all('a')
        for industry in industries:
            href = industry['href']
            links.append((industry.get_text(), href))
        return links
    
    @property
    def categories(self):
        links = list()
        industries = self.soup.find('div', class_='m2l') \
                            .find('div', class_='sort') \
                            .find_all('a')
        for industry in industries:
            href = industry['href']
            links.append((industry.get_text(), href))
        return links
    
    @property
    def pages(self):
        res = self.soup.find('div', class_='pages')
        if res is not None:
            return os.path.splitext(res.select_one('a')['href'])
        else:
            return os.path.splitext(self.href)

    def get_content(self):
        company = list()
        res = self.soup.find_all('div', class_='list')
        for data in res:
            val = data.find_all('a')[-1]
            cmp_name, href = val.get_text(), val['href']
            if data.find('img', alt='VIP') is not None:
                vip = 1
            else:
                vip = 0
            print(cmp_name, href)
            if self.get_detail(cmp_name, href, vip) is not None:
                company.append(self.get_detail(cmp_name, href, vip))
        return company

    def get_detail(self, name, href, vip):
        company_dict = dict()
        company_dict['公司名稱'] = name
        company_dict['VIP'] = vip
        try:
            response = requests.get(href)
            soup = BeautifulSoup(response.text, 'html.parser')
            filter_ = list(['聯繫人', '聯絡電話', '所在地', '地址', '主營行業'])
            res = soup.find('div', class_='contact_body').find_all('li')
            for data in res:
                for val in filter_:
                    if val in str(data):
                        original_str = data.get_text()
                        new_str = original_str.replace(val, '')
                        for sign in ['\n', '\xa0']:
                            new_str = new_str.replace(sign, '')
                        company_dict[val] = new_str
                    else:
                        pass
            for val in filter_:
                if val not in company_dict.keys():
                    company_dict[val] = ''
        except:
            try:
                name_dict = {
                    '聯 絡 人：': '聯繫人',
                    '所在地區：': '所在地',
                    '公司地址：': '地址'
                }
                response = requests.get(href+'&file=contact')
                soup = BeautifulSoup(response.text, 'html.parser')
                filter_ = list(['聯繫人', '聯絡電話', '所在地', '地址', '主營行業'])
                replace_filter = list(['聯 絡 人：', '所在地區：', '公司地址：'])
                res = soup.find('div', class_='px14 lh18').find_all('tr')
                for data in res:
                    for val in replace_filter:
                        if val in str(data):
                            original_str = data.get_text()
                            new_str = original_str.replace(val, '')
                            for sign in ['\n', '\xa0']:
                                new_str = new_str.replace(sign, '')
                            company_dict[name_dict[val]] = new_str
                for val in filter_:
                    if val not in company_dict.keys():
                        company_dict[val] = ''
            except:
                print('company not match.')
        return company_dict

if __name__ == '__main__':
    crawler = buildingCrawler()
    main_industries = crawler.main_industries_href
    for main_industry, main_href in main_industries:
        crawler.get_response(main_href)
        sub_industries = crawler.sub_industries_href
        for sub_industry, sub_href in sub_industries:
            print('{} -> {}'.format(main_industry, sub_industry))
            crawler.get_response(sub_href)
            website, extension = crawler.pages
            if website is not None:
                for page in range(1, int(website.split('-')[-1])+1):
                    crawler.get_response('-'.join(website.split('-')[:-1])+'-'+str(page)+extension)
                    for data in crawler.get_content():
                        data['主產業類別'] = main_industry
                        data['子產業類別'] = sub_industry
                        output = dict(sorted(data.items(), key=lambda item: item[0]))
                        try:
                            with open('./building.csv', 'a') as f:
                                fieldnames = ['主產業類別', '子產業類別', 'VIP','公司名稱', '聯繫人', 
                                                '聯絡電話', '所在地', '地址', '主營行業']
                                w = csv.DictWriter(f, fieldnames=fieldnames)
                                w.writerow(output)
                        except:
                            pass
