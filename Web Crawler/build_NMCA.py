import re
import os
import csv
import sys
import time
import requests

from bs4 import BeautifulSoup
from dataclasses import dataclass, field

@dataclass
class buildingCrawler:
    html: str = ''

    @property
    def website(self) -> str:
        return self.html
    
    @website.setter
    def website(self, value):
        self.html = value

    def get_response(self, href):
        self.href = href
        self.response = requests.get(href)
        self.response.encoding='Big5'
        self.soup = BeautifulSoup(self.response.text, 'html.parser')

    @property
    def main_industries_href(self):
        links = list()
        industries = self.soup.find_all('option')
        for industry in industries:
            if 'Tsoft' in industry['value']:
                links.append((industry.get_text(), self.href.split('?')[0]+'{}'.format(industry['value'])))
        return links
    
    @property
    def pages(self):
        links = list()
        links.append(self.href+'&list_p=0')
        
        res = self.soup.find_all('a')
        for a_tag in res:
            if 'list_p=' in a_tag['href']:
                links.append(self.href.split('?')[0]+a_tag['href'])
        return links

    def get_content(self):
        company = list()
        res = self.soup.find_all('td', attrs={'colspan': '2'})
        for td_tag in res:
            if td_tag.find('font', attrs={'size': '3'}) is not None:
                a = td_tag.select('a')[0]
                cmp_name, href = a.get_text(), self.href.split('?')[0]+a['href']
                print(cmp_name, href)
                if self.get_detail(cmp_name, href) is not None:
                    company.append(self.get_detail(cmp_name, href))
        return company

    def get_detail(self, name, href):
        company_dict = dict()
        company_dict['公司名稱'] = name
        filter_ = ['聯  絡  人', '聯絡電話', '通訊地址']

        response = requests.get(href)
        response.encoding='Big5'
        soup = BeautifulSoup(response.text, 'html.parser')
        key = soup.find_all('td', attrs={'width': ['12%']})
        val = soup.find_all('td', attrs={'width': ['35%', '41%']})
        for k, v in zip(key, val):
            if any(filter in k.get_text() for filter in filter_):
                company_dict[k.get_text().replace('\xa0 ', '').replace('：', '')] = v.get_text()
        return company_dict

if __name__ == '__main__':
    crawler = buildingCrawler()
    website = 'http://www.nca-machine.org.tw/?Tsoft=1855&p=zsoft'
    crawler.get_response(website)
    main_industries = crawler.main_industries_href
    for main_industry, main_href in main_industries:
        crawler.get_response(main_href)
        pages = crawler.pages
        for page_href in pages:
            print('{} -> {}'.format(main_industry, page_href))
            crawler.get_response(page_href)
            for data in crawler.get_content():
                data['產業類別'] = main_industry
                output = dict(sorted(data.items(), key=lambda item: item[0]))
                try:
                    with open('./building.csv', 'a', encoding="utf-8-sig", newline='') as f:
                        fieldnames = ['產業類別', '公司名稱', '聯絡人', '聯絡電話', '通訊地址']
                        w = csv.DictWriter(f, fieldnames=fieldnames)
                        w.writerow(output)
                except:
                    pass
        os.rename('./building.csv', './{}.csv'.format(main_industry))
                
