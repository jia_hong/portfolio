import re
import os
import csv
import sys
import time
import requests

from bs4 import BeautifulSoup
from dataclasses import dataclass, field

@dataclass
class buildingCrawler:
    html: str = 'http://www.tbmta.com.tw/?p=zsoft&Tsoft=6292'

    def __post_init__(self):
        self.get_response(self.html)

    def get_response(self, href):
        self.href = href
        self.response = requests.get(href)
        self.response.encoding='Big5'
        self.soup = BeautifulSoup(self.response.text, 'html.parser')

    @property
    def industries_href(self):
        links = list()
        industries = self.soup.find_all('option')
        for industry in industries:
            if 'Tsoft' in industry['value']:
                links.append((industry.get_text(), 
                                'http://www.tbmta.com.tw/'+industry['value']))
        return links
    
    @property
    def elements(self):
        res = self.soup.find('tr', attrs={'bgcolor': 'EEEEEE'})
        if res == None:
            return None
        else:
            return res.find('font', attrs={'color': 'red'}).get_text()

    def get_content(self):
        company = list()
        res = self.soup.find_all('table', attrs={'width': '96%'})
        for table in res:
            for a_tag in table.select('a'):
                if 'p=zshow' in a_tag['href']:
                    cmp_name, href = a_tag.get_text(), 'http://www.tbmta.com.tw/'+a_tag['href']
                    print(cmp_name, href)
                    if self.get_detail(cmp_name, href) is not None:
                        company.append(self.get_detail(cmp_name, href))
        return company
    
    def get_detail(self, name, href):
        company_dict = dict()
        response = requests.get(href)
        response.encoding='Big5'
        soup = BeautifulSoup(response.text, 'html.parser')
        key = [re.sub('[： \xa0]', '', val.get_text()) for val in soup.find_all("td", attrs={'width': '12%'})]
        name, address = [val.get_text() for val in soup.find_all("td", attrs={'width': '41%'})]
        cmp_name, tel, _ = [val.get_text() for val in soup.find_all("td", attrs={'width': '35%'})]
        company_dict['公司名稱'] = cmp_name
        company_dict['聯絡人'] = name
        company_dict['通訊地址'] = address
        company_dict['聯絡電話'] = tel
        return company_dict

if __name__ == '__main__':
    crawler = buildingCrawler()
    industries = crawler.industries_href
    for industry, href in industries:
        crawler.get_response(href)
        elements = crawler.elements
        if not elements == None:
            for element in range(0, int(elements), 20):
                print(href+'&list_p={}'.format(element), int(elements))
                crawler.get_response(href+'&list_p={}'.format(element))
                for data in crawler.get_content():
                    data['產業類別'] = industry
                    output = dict(sorted(data.items(), key=lambda item: item[0]))
                    try:
                        with open('./building_TBMTA.csv', 'a', newline='') as f:
                            fieldnames = ['產業類別', '公司名稱', '聯絡人', '聯絡電話', '通訊地址']
                            w = csv.DictWriter(f, fieldnames=fieldnames)
                            w.writerow(output)
                    except:
                        pass
        else:
            crawler.get_response(href+'&list_p=0')
            for data in crawler.get_content():
                    data['產業類別'] = industry
                    output = dict(sorted(data.items(), key=lambda item: item[0]))
                    try:
                        with open('./building_TBMTA.csv', 'a', newline='') as f:
                            fieldnames = ['產業類別', '公司名稱', '聯絡人', '聯絡電話', '通訊地址']
                            w = csv.DictWriter(f, fieldnames=fieldnames)
                            w.writerow(output)
                    except:
                        pass