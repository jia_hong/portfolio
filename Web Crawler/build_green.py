import re
import os
import csv
import sys
import time
import requests

from bs4 import BeautifulSoup
from dataclasses import dataclass, field

@dataclass
class buildingCrawler:
    html: str = ''

    @property
    def website(self) -> str:
        return self.html
    
    @website.setter
    def website(self, value):
        self.html = value

    def get_response(self, href):
        self.href = href
        self.response = requests.get(href)
        self.soup = BeautifulSoup(self.response.text, 'html.parser')

    def get_content(self):
        company = list()
        res = self.soup.find_all('a')
        for a_tag in res:
            if 'dataid' in a_tag['href']:
                cmp_name, href = a_tag.get_text(), 'http://mgr.tabc.org.tw'+a_tag['href']
                print(cmp_name, href)
                if self.get_detail(cmp_name, href) is not None:
                    company.append(self.get_detail(cmp_name, href))
        return company

    def get_detail(self, name, href):
        company_dict = dict()
        response = requests.get(href)
        soup = BeautifulSoup(response.text, 'html.parser')
        div = soup.find("div", {"class": "contentfi"})
        
        company_key = list()
        company_val = list()
        for idx, td_tag in enumerate(div.select('td')):
            if idx % 2 == 0:
                company_key.append(re.sub('：', '', td_tag.get_text()))
            else:
                company_val.append(re.sub('[\n\t\r\xa0]', '', td_tag.get_text()))
        for key, val in zip(company_key, company_val):
            if not key == '生產國別':
                company_dict[key] = val
        return company_dict


if __name__ == '__main__':
    crawler = buildingCrawler()
    for page in range(1, 78):
        crawler.get_response('http://mgr.tabc.org.tw/tabcMgr/gbm_op/searchCaseAction.do?res=C4C89664500E03F5E050007F010173F1&d-16544-p={}'.format(page))
        for data in crawler.get_content():
            output = dict(sorted(data.items(), key=lambda item: item[0]))
            try:
                with open('./green_building.csv', 'a', newline='') as f:
                    fieldnames = ['公司名稱', '負責人', '聯絡人', '地址', '電話', '傳真', 'Email']
                    w = csv.DictWriter(f, fieldnames=fieldnames)
                    w.writerow(output)
            except:
                pass
