import os
import sys
import sympy
import numpy as np
import matplotlib.pyplot as plt

from typing import List
from dataclasses import dataclass
from matplotlib.patches import Ellipse
import matplotlib.animation as animation

@dataclass
class Point:
    x: float = 0.0
    y: float = 0.0

@dataclass
class Line:
    slope: float
    intercept: float

@dataclass
class Oval:
    center: Point
    long_axis: float
    short_axis: float

@dataclass
class Reflection:
    oval: Oval
    line: Line
    point: Point
    def __post_init__(self):
        if __debug__:
            if not np.round(self.point.y, 6) == np.round(self.line.slope*self.point.x+self.line.intercept,6):
                raise AssertionError('point must be your linear equation solution.')

    def get_dist(self, x, y):
        return ((self.point.x-x)**2 + (self.point.y-y)**2)**0.5

    def get_cross_point(self):
        solution_set = list()
        x = sympy.Symbol('x')
        y = sympy.Symbol('y')

        linear_equation = self.line.slope*x + self.line.intercept - y
        positive_oval = self.oval.short_axis*(1 - x**2 / self.oval.long_axis**2)**0.5 - y
        negative_oval = -self.oval.short_axis*(1 - x**2 / self.oval.long_axis**2)**0.5 - y
        solution_set.extend(sympy.solve([positive_oval, linear_equation], [x, y]))
        solution_set.extend(sympy.solve([negative_oval, linear_equation], [x, y]))
        return solution_set

    def get_Quadrant(self, x, y):
        if x > 0 and y > 0:
            return '1'
        elif x < 0 and y > 0:
            return '2'
        elif x < 0 and y < 0:
            return '3'
        elif x > 0 and y < 0:
            return '4'

    def get_tangent_line(self):
        oval = self.oval
        x = sympy.Symbol('x')
        y = sympy.Symbol('y')
        if any([self.get_Quadrant(self.point.x, self.point.y) == th for th in ['1', '3']]):
            slope = sympy.diff(-oval.short_axis*(1 - x**2 / oval.long_axis**2)**0.5, x).evalf(subs={'x': self.point.x})
        elif any([self.get_Quadrant(self.point.x, self.point.y) == th for th in ['2', '4']]):
            slope = sympy.diff(oval.short_axis*(1 - x**2 / oval.long_axis**2)**0.5, x).evalf(subs={'x': self.point.x})
        intercept = self.point.y - slope*self.point.x
        return Line(slope, intercept)
    
    def get_perpend_line(self, tangent_line):
        slope = -1/tangent_line.slope
        intercept = self.point.y - slope*self.point.x
        return Line(slope, intercept)

    def get_reflection_line(self, perpend_line):
        prop = (perpend_line.slope-self.line.slope)/(1+perpend_line.slope*self.line.slope)
        slope = (perpend_line.slope+prop)/(1-perpend_line.slope*prop)
        intercept = self.point.y - slope*self.point.x
        return Line(slope, intercept)

@dataclass
class graphic:
    reflection: Reflection
    def __post_init__(self):
        self.oval = self.reflection.oval
        self.fig = plt.figure(figsize=(10,9),facecolor = 'black')
        self.ax = self.fig.add_subplot(111, facecolor = 'black')
        e = Ellipse((self.oval.center.x, self.oval.center.y), width = self.oval.long_axis*2, height = self.oval.short_axis*2, 
                        fc= 'none', ec='blue')
        self.ax.add_patch(e)

    def get_data(self):
        reflection = self.reflection
        while True:
            dist = [(reflection.get_dist(x, y), (x, y)) for (x, y) in reflection.get_cross_point()]
            dist.sort(key= lambda x:x[0], reverse=True)
            _, (x, y) = dist[0]
            data_x = [reflection.point.x, x]
            data_y = [reflection.point.y, y]
            reflection.point = Point(x, y)
            tangent_line = reflection.get_tangent_line()
            perpend_line = reflection.get_perpend_line(tangent_line)
            reflection_line = reflection.get_reflection_line(perpend_line)
            reflection.line = reflection_line
            yield(data_x, data_y)

    def update(self, i):
        xss, yss = next(self.get_data())
        self.ax.plot(xss, yss, color='green', alpha=0.7,lw=1)
        return self.ax

    def animate(self):
        self.ani = animation.FuncAnimation(self.fig, self.update, frames=1, interval=0.01, blit=False, repeat = True)
        plt.show()

if __name__ == '__main__':
    a = 10 #长轴
    b = 8
    q = 0.35  #初始位置(x0,y0)横坐标在(-a,a)区间的百分比位置
    x0 = -a + q * (a - (-a))  #(-a,a)
    y0 = -b*np.sqrt(1 - x0**2/a**2) #初始位置出现在椭圆下半球
    #初始化发射方向（初始射线斜率和截距）
    k0 = -10 #初始射线斜率
    m0 = y0 - k0*x0 #初始射线斜率截距
    reflection = Reflection(Oval(Point(0, 0), a, b), Line(k0, m0), Point(x0, y0))
    graphic = graphic(reflection)
    graphic.animate()