import numpy as np
import pandas as pd

from lib.debugging import AverageMeter, ProgressMeter
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

class construct_dataset(object):
    def __init__(self, dataset):
        self._dataset = dataset
        self.drop_null()
        self.preprocessing()

    def drop_null(self):
        self._dataset.dropna(axis=0, how='any', inplace=True)

    def preprocessing(self):
        self.drop_invalid_parameter()
        self.label_encoder()

    def drop_invalid_parameter(self):
        for parameter in ['Loan_ID', 'Property_Area']:
            self._dataset.drop(parameter, axis=1, inplace=True)

    def label_encoder(self):
        obj_col = self._dataset.select_dtypes('object').columns
        le = LabelEncoder()
        for col in obj_col:
            self._dataset[col] = le.fit_transform(self._dataset[col])

    def standard_scaler(self, dataset):
        scaler = StandardScaler()
        return scaler.fit_transform(dataset)

    @property
    def dataset(self):
        return self.standard_scaler(self._dataset.iloc[:, :-1])
    
    @property
    def target(self):
        return self._dataset.iloc[:, -1]

if __name__ == '__main__':
    loan_prediction_for_train = pd.read_csv('./train.csv')
    
    train = construct_dataset(loan_prediction_for_train)
    train_x, val_x, train_y, val_y = train_test_split(train.dataset, train.target, test_size = 1/3, random_state = 0)

    forest  = RandomForestClassifier(n_estimators=200)
    forest.fit(train_x, train_y)
    pred_y = forest.predict(val_x)
    
    print('acc: {:.2f}%'.format(accuracy_score(val_y, pred_y)*100))
    print('percision : {:.2f}%'.format(precision_score(val_y, pred_y)*100))
    print('recall : {:.2f}%'.format(recall_score(val_y, pred_y)*100))
    print('f1_score: {:.2f}%'.format(f1_score(val_y, pred_y)*100))



