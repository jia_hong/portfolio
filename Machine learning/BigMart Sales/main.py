import math
import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error

class construct_dataset(object):
    def __init__(self, dataset):
        self._dataset = dataset
        self.fill_null()
        self.preprocessing()

    def fill_null(self):
        self._dataset['Outlet_Size'].fillna(self._dataset['Outlet_Size'].mode()[0], inplace=True)
        self._dataset['Item_Weight'].fillna(self._dataset['Item_Weight'].median(), inplace=True)

    def preprocessing(self):
        self.drop_invalid_parameter()
        self.label_encoder()

    def drop_invalid_parameter(self):
        parameters = ['Item_Visibility', 'Outlet_Size', 'Outlet_Establishment_Year', 
                        'Outlet_Type', 'Item_Weight', 'Item_Identifier']
        for parameter in parameters:
            self._dataset.drop(parameter, axis=1, inplace=True)

    def label_encoder(self):
        obj_col = self._dataset.select_dtypes('object').columns
        le = LabelEncoder()
        for col in obj_col:
            self._dataset[col] = le.fit_transform(self._dataset[col])

    def standard_scaler(self, dataset):
        scaler = StandardScaler()
        return scaler.fit_transform(dataset)

    @property
    def dataset(self):
        return self.standard_scaler(self._dataset.iloc[:, :-1])
    
    @property
    def target(self):
        return self._dataset.iloc[:, -1]

if __name__ == '__main__':
    bigmart_slaes_for_train = pd.read_csv('./train.csv')
    
    train = construct_dataset(bigmart_slaes_for_train)
    train_x, val_x, train_y, val_y = train_test_split(train.dataset, train.target, test_size = 1/3 )

    lr = LinearRegression()
    lr.fit(train_x, train_y)
    pred_y = lr.predict(val_x)

    print("R2 Score:", r2_score(val_y, pred_y))
    print("Mean Squarred Error:", mean_squared_error(val_y, pred_y))
    print("RMSE:", math.sqrt(mean_squared_error(val_y, pred_y)))
    print("Mean Absolute Error : {}".format(mean_absolute_error(val_y,pred_y)))