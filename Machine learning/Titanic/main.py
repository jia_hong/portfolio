import numpy as np
import pandas as pd

from sklearn.svm import SVC
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.model_selection import train_test_split

from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

class construct_dataset(object):
    def __init__(self, dataset):
        self._dataset = dataset
        self.drop_null()
        # self.fill_null()
        self.drop_invalid_parameter()
        self.label_encoder()

    def fill_null(self):
        self._dataset['Age'].fillna(self._dataset['Age'].median(), inplace=True)

    def drop_null(self):
        self._dataset.dropna(axis=0, how='any', inplace=True)

    def drop_invalid_parameter(self):
        col_names = ['PassengerId', 'Name', 'SibSp', 'Parch', 'Ticket', 'Cabin']
        for parameter in col_names:
            self._dataset.drop(parameter, axis=1, inplace=True)

    def label_encoder(self):
        obj_col = self._dataset.select_dtypes('object').columns
        le = LabelEncoder()
        for col in obj_col:
            self._dataset[col] = le.fit_transform(self._dataset[col])

    def standard_scaler(self, dataset):
        scaler = StandardScaler()
        return scaler.fit_transform(dataset)

    @property
    def dataset(self):
        return self._dataset.iloc[:, 1:]
    
    @property
    def target(self):
        return self._dataset.iloc[:, 0]

def classification(svm):
    titanic_for_test = pd.read_csv('./test.csv')

    titanic_for_test['Age'].fillna(titanic_for_test['Age'].median(), inplace=True)
    titanic_for_test['Fare'].fillna(titanic_for_test['Fare'].mean(), inplace=True)
    titanic_for_test['Embarked'].fillna(titanic_for_test['Embarked'].mode()[0], inplace=True)

    pred_dict = dict()
    pred_dict['PassengerId'] = titanic_for_test['PassengerId']

    col_names = ['PassengerId', 'Name', 'SibSp', 'Parch', 'Ticket', 'Cabin']
    for parameter in col_names:
        titanic_for_test.drop(parameter, axis=1, inplace=True)
    
    obj_col = titanic_for_test.select_dtypes('object').columns
    le = LabelEncoder()
    for col in obj_col:
        titanic_for_test[col] = le.fit_transform(titanic_for_test[col])
    
    pred_y = svm.predict(titanic_for_test.values)
    pred_dict['Survived'] = pred_y
    
    res = pd.DataFrame(pred_dict)
    res.to_csv('Submission.csv', index=False)

if __name__ == '__main__':
    titanic_for_train = pd.read_csv('./train.csv')

    train = construct_dataset(titanic_for_train)
    train_x, val_x, train_y, val_y = train_test_split(train.dataset, train.target, test_size=1/3, random_state=0)

    svm = SVC(kernel='linear', probability=True)
    svm.fit(train_x, train_y)

    pred_y = svm.predict(val_x)

    print('acc: {:.2f}%'.format(accuracy_score(val_y, pred_y)*100))
    print('percision : {:.2f}%'.format(precision_score(val_y, pred_y)*100))
    print('recall : {:.2f}%'.format(recall_score(val_y, pred_y)*100))
    print('f1_score: {:.2f}%'.format(f1_score(val_y, pred_y)*100))

    classification(svm)

