import time
import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from torch.utils.data import DataLoader
from sklearn import datasets
from sklearn.model_selection import train_test_split

class Dataset:
    def __init__(self, data, target):
        self.data = torch.FloatTensor(data)
        self.target = torch.LongTensor(target)
    
    def __getitem__(self, index):
        return self.data[index], self.target[index]
    
    def __len__(self):
        return len(self.data)

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear(4, 32)
        self.fc2 = nn.Linear(32, 64)
        self.fc3 = nn.Linear(64, 3)
    
    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x

class AverageMeter(object):
    def __init__(self, name, fmt=':f'):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = '{name} {val' + self.fmt + '} ({avg' + self.fmt + '})'
        return fmtstr.format(**self.__dict__)

class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print('\t'.join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = '{:' + str(num_digits) + 'd}'
        return '[' + fmt + '/' + fmt.format(num_batches) + ']'

def accuracy(outputs, targets, topk=(1,)):
    maxk = max(topk)
    batch_size = targets.size(0)
    _, pred = outputs.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(targets.view(1, -1).expand_as(pred))

    res = list()
    for k in topk:
        correct_k = correct[:k].reshape(-1).float().sum(0, keepdim=True)
        res.append(correct_k.mul_(100.0 / batch_size))
    return res

def train(train_loader, model, criterion, optimizer, device, epoch):
    losses = AverageMeter('loss', ':.4e')
    top1 = AverageMeter('acc@1', ':6.2f')
    progress = ProgressMeter(
        len(train_loader),
        [losses, top1],
        prefix="Epoch: [{}]".format(epoch))

    model.train()
    for i, (inputs, targets) in enumerate(train_loader):
        inputs = inputs.to(device)
        targets = targets.to(device)

        optimizer.zero_grad()

        outputs = model(inputs)
        loss = criterion(outputs, targets)

        topDict = dict()
        for idx, acc in enumerate(accuracy(outputs, targets, topk=(1,))):
            topDict['top{}'.format(idx+1)] = acc[0]
        losses.update(loss.item(), inputs.size(0))
        top1.update(topDict['top1'], inputs.size(0))

        loss.backward()
        optimizer.step()
        progress.display(i)

def validation(val_loader, model, criterion, device, epoch):
    losses = AverageMeter('loss', ':.4e')
    top1 = AverageMeter('acc@1', ':6.2f')
    progress = ProgressMeter(
        len(val_loader),
        [losses, top1],
        prefix='Test: ')

    model.eval()
    with torch.no_grad():
        for i, (inputs, targets) in enumerate(val_loader):
            inputs = inputs.to(device)
            targets = targets.to(device)

            optimizer.zero_grad()

            outputs = model(inputs)
            loss = criterion(outputs, targets)

            topDict = dict()
            for idx, acc in enumerate(accuracy(outputs, targets, topk=(1,))):
                topDict['top{}'.format(idx+1)] = acc[0]
            losses.update(loss.item(), inputs.size(0))
            top1.update(topDict['top1'], inputs.size(0))
        progress.display(i)
        print(' * Acc@1 {top1.avg:.3f}'.format(top1=top1))

if __name__ == '__main__':
    iris = datasets.load_iris()
    datasets = iris.data
    labels = iris.target

    train_x, val_x, train_y, val_y = train_test_split(datasets, labels, test_size=1/3, random_state=0)

    train_loader = DataLoader(
        Dataset(train_x, train_y),
        batch_size=64, 
        shuffle=True,
        pin_memory=True
    )

    val_loader = DataLoader(
        Dataset(val_x, val_y),
        batch_size=64, 
        shuffle=True,
        pin_memory=True
    )

    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = Net().to(device)

    criterion = nn.CrossEntropyLoss().to(device)
    optimizer = torch.optim.SGD(model.parameters(), lr=0.01, momentum=0.9)

    for epoch in range(100):
        train(train_loader, model, criterion, optimizer, device, epoch)
        validation(val_loader, model, criterion, device, epoch)