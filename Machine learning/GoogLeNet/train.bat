for /l %%x in (77, 1, 77) do (
   python train_for_one_class.py --database_name=AUO --collection_name=CKPIC405_processed --model_number=T650QVN07 ^
                   --times=%%x --auto_analyze=True ^
                   --normalization_file=./normalization/analyze/normalization_times_%%x.json ^
                   --feature_file=./feature/contiune/feature_times_%%x.cfg ^
                   --classification_file=./classification/contiune/times_%%x_analyze/ ^
                   --figure_file=./figure/contiune/times_%%x_analyze/ ^
                   --tmp_model_dir=./model/contiune/times_%%x_analyze/ ^
                   --training_mode=contiune
   
   python train_for_one_class.py --database_name=AUO --collection_name=CKPIC405_processed --model_number=T650QVN07 ^
                   --times=%%x --auto_analyze=False ^
                   --normalization_file=./normalization/contiune/no_analyze/normalization_times_%%x.json ^
                   --classification_file=./classification/contiune/times_%%x_no_analyze/ ^
                   --figure_file=./figure/contiune/times_%%x_no_analyze/ ^
                   --tmp_model_dir=./model/contiune/times_%%x_no_analyze/ ^
                   --training_mode=contiune

   python train_for_one_class.py --database_name=AUO --collection_name=CKPIC405_processed --model_number=T650QVN07 ^
                   --times=%%x --auto_analyze=True ^
                   --normalization_file=./normalization/no_contiune/analyze/normalization_times_%%x.json ^
                    --classification_file=./classification/no_contiune/times_%%x_analyze/ ^
                   --figure_file=./figure/no_contiune/times_%%x_analyze/ ^
                   --feature_file=./feature/no_contiune/feature_times_%%x.cfg ^
                   --tmp_model_dir=./model/no_contiune/times_%%x_analyze/ ^
                   --training_mode=no_contiune
   
   python train_for_one_class.py --database_name=AUO --collection_name=CKPIC405_processed --model_number=T650QVN07 ^
                   --times=%%x --auto_analyze=False ^
                   --normalization_file=./normalization/no_contiune/no_analyze/normalization_times_%%x.json ^
                    --classification_file=./classification/no_contiune/times_%%x_no_analyze/ ^
                   --figure_file=./figure/no_contiune/times_%%x_no_analyze/ ^
                   --tmp_model_dir=./model/no_contiune/times_%%x_no_analyze/ ^
                   --training_mode=no_contiune
)
