import os
import time
import shutil
import argparse
import numpy as np
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--dir', default='./', type=str, help='dataset directory')
parser.add_argument('-t', '--train_split', default=0.8, type=float,
                    help='The ratio of training set')
parser.add_argument('-v', '--val_split', default=0.2, type=float,
                    help='The ratio of validation set')

if __name__ == '__main__':
    global args
    args = parser.parse_args()
    assert args.train_split + args.val_split == 1.0

    if not os.path.isdir('./data'):
        os.mkdir('./data')
        if not os.path.isdir('./data/train'):
            os.mkdir('./data/train')
        if not os.path.isdir('./data/val'):
            os.mkdir('./data/val')
    else:
        pass

    for root, dirs, files in os.walk(args.dir):
        for _dir in dirs:
            _files = os.listdir(os.path.join(root, _dir))
            np.random.shuffle(_files)
            train_num = int(len(_files)*args.train_split)
            training_set = _files[:train_num]
            validation_set = _files[train_num:]

            tqdm_train = tqdm(training_set)
            tqdm_validation = tqdm(validation_set)

            for file in tqdm_train:
                tqdm_train.set_description('Training set ({})'.format(_dir))
                if not os.path.isdir(os.path.join('./data/train', _dir)):
                    os.mkdir(os.path.join('./data/train', _dir))
                shutil.copy(os.path.join(root, _dir, file), os.path.join('./data/train', _dir))

            for file in tqdm_validation:
                tqdm_validation.set_description('validation set ({})'.format(_dir))
                if not os.path.isdir(os.path.join('./data/val', _dir)):
                    os.mkdir(os.path.join('./data/val', _dir))
                shutil.copy(os.path.join(root, _dir, file), os.path.join('./data/val', _dir))