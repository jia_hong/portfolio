import os
import sys
import warnings

from PIL import Image

if __name__ == "__main__":
    warnings.filterwarnings("error", category=UserWarning)
    for root, dirs, files in os.walk('./PetImages'):
        for f in files:         
            try:
                img = Image.open(os.path.join(root, f))
            except:
                print(os.path.join(root, f))
