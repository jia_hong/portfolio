import os
import sys
import math
import time
import shutil
import argparse
import numpy as np
import pandas as pd

import torch
import torch.optim
import torch.nn as nn
import torch.utils.data
import torch.nn.parallel
import torch.nn.functional as F
import torch.backends.cudnn as cudnn

from torchsummary import summary

from datetime import datetime, timedelta

from Class.AUO import AUO
from Class.graph import plot
from Class.db import MongoDB

from Net.Inceptionv4 import InceptionV4

parser = argparse.ArgumentParser(description='PyTorch Testing')
parser.add_argument('--database_name', type=str, help='database name.')
parser.add_argument('--collection_name', type=str)
parser.add_argument('--model_number', help='product number')

parser.add_argument('--miss', type=int, help='training datasets miss')
parser.add_argument('--freq', type=int, help='training datasets frequency')
parser.add_argument('--pre_count', default=60, type=int, help='pre count normal regard as fail (default: 20)')
parser.add_argument('--time_range', default=20, type=int, help='time range (default: 20)')
parser.add_argument('--feature_file', type=str, default='./')
parser.add_argument('--normalization_file', type=str)

parser.add_argument('--model_dir', default='./model', help='save model path')

def resize(x, inputs):
        (height, width) = inputs
        rows, cols = x.shape
        resized_x = np.zeros((height, width))

        for row in range(rows):
            for col in range(cols):
                n_rows = row*math.ceil(height/rows)
                n_cols = col*math.ceil(width/cols)
                resized_x[n_rows:n_rows+int(height/rows)+1, n_cols:n_cols+int(width/cols)+1] = x[row][col]
        return resized_x

def create_DataFrame(db, features):
    df = pd.DataFrame(data = db.find({'MODEL_NO': args.model_number}), columns = np.hstack((features, np.array(['REPORT_TIME', 'label']))))
    df.dropna(how='any', axis=0, inplace=True)
    return df

def constract_model():
    cudnn.benchmark = True

    checkpoint = torch.load(args.model_dir)
    model = InceptionV4(num_classes=2)

    model.load_state_dict(checkpoint['model_state_dict'])

    if torch.cuda.is_available():
        model.cuda()

    classification(model)

def classification(model):
    db = MongoDB(args.database_name, args.collection_name)

    features = list()
    with open(args.feature_file, 'r') as f:
        for line in f:
            features.append(line[:-1])

    df = create_DataFrame(db, features)
    db.disconnect()
    col_names = df.columns

    datasets_for_testing = np.empty((0, df.columns.size))

    miss = args.miss
    frequency = args.freq
    for _, grouped in df.groupby(pd.Grouper(key='REPORT_TIME', freq='D')):
        if not grouped.empty:
            if miss > 0:
                miss -= 1
            elif frequency > 0:
                frequency -= 1
            else:
                datasets_for_testing = np.vstack((datasets_for_testing, grouped.iloc[:, :]))
                break

    testing_AUO = AUO(pd.DataFrame(datasets_for_testing, columns=col_names), args.pre_count, args.time_range, len(features), 
                        'test', args.normalization_file)
    testing_df_dict = testing_AUO.split_timeline()
    testing_datasets, testing_labels, testing_time_line = testing_AUO.AUO_testing_datasets(testing_df_dict)

    predicted = list()
    pre_predicted = list()
    inter_quartile = np.array([0.25, 0.5, 0.75, 1])

    model.eval()
    with torch.no_grad():
        for i, (input_, label, time) in enumerate(zip(testing_datasets, testing_labels, testing_time_line)):
            input_ = resize(input_, (299, 299))
            input_ = torch.from_numpy(input_)
            input_ = input_.unsqueeze(0).unsqueeze(0)
            input_ = input_.expand(-1, 3, -1, -1).type(torch.FloatTensor)
            input_ = input_.cuda()

            outputs = model(input_)
            percentage = torch.nn.functional.softmax(outputs, dim=1)[0]
            pre_predicted.append(percentage[1].item())
            if len(pre_predicted) > 4:
                del pre_predicted[0]
                predicted.append(np.sum(inter_quartile*pre_predicted)/np.sum(inter_quartile))
            else:
                predicted.append(percentage[1].item())

    breakpoints = list()
    time_line = testing_time_line.astype(np.datetime64, copy=False)
    for idx, val in enumerate(time_line - np.roll(time_line, 1)):
        if val > np.timedelta64(120, 's'):
            breakpoints.append(idx)

    testing_time_line = np.insert(testing_time_line, breakpoints, np.nan, axis=0)
    predicted = np.insert(np.asarray(predicted), breakpoints, np.nan, axis=0)
    testing_labels = np.insert(testing_labels, breakpoints, np.nan, axis=0)

    fig = plot('./test.html', testing_time_line, predicted, testing_labels)
    fig.line_and_mark()
    fig.save()

if __name__ == "__main__":
    global args

    args = parser.parse_args()

    constract_model()