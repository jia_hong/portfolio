from __future__ import print_function, division, absolute_import
import torch
import torch.nn as nn
import torch.nn.functional as F
import os
import sys

class BasicConv2d(nn.Module):
    def __init__(self, in_planes, out_planes, kernel_size, stride, padding=0):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_planes, out_planes,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, bias=False)
        self.bn = nn.BatchNorm2d(out_planes,
                                 eps=0.001,
                                 momentum=0.1,
                                 affine=True)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x

class Stem_block_1(nn.Module):
    def __init__(self):
        super(Stem_block_1, self).__init__()
        self.branch0 = nn.MaxPool2d(3, stride=2)

        self.branch1 = BasicConv2d(64, 96, kernel_size=3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)

        out = torch.cat((x0, x1), 1)

        return out

class Stem_block_2(nn.Module):
    def __init__(self):
        super(Stem_block_2, self).__init__()
        self.branch0 = nn.Sequential(
            BasicConv2d(160, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 96, kernel_size=3, stride=1)
        )

        self.branch1 = nn.Sequential(
            BasicConv2d(160, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 64, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(64, 64, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(64, 96, kernel_size=3, stride=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)

        out = torch.cat((x0, x1), 1)

        return out

class Stem_block_3(nn.Module):
    def __init__(self):
        super(Stem_block_3, self).__init__()
        self.branch0 = nn.MaxPool2d(3, stride=2)

        self.branch1 = BasicConv2d(192, 192, kernel_size=3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)

        out = torch.cat((x0, x1), 1)

        return out

class Inception_resnet_A(nn.Module):
    def __init__(self, scale=0.1):
        super(Inception_resnet_A, self).__init__()
        self.scale = scale

        self.branch0 = BasicConv2d(384, 32, kernel_size=1, stride=1)
        
        self.branch1 = nn.Sequential(
            BasicConv2d(384, 32, kernel_size=1, stride=1),
            BasicConv2d(32, 32, kernel_size=3, stride=1, padding=1)
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(384, 32, kernel_size=1, stride=1),
            BasicConv2d(32, 48, kernel_size=3, stride=1, padding=1),
            BasicConv2d(48, 64, kernel_size=3, stride=1, padding=1)
        )

        self.conv2d = nn.Conv2d(128, 384, kernel_size=1, stride=1)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        out = torch.cat((x0, x1, x2), 1)
        out =self.conv2d(out)
        out = x + self.scale * out
        out = self.relu(out)

        return out

class Inception_resnet_B(nn.Module):
    def __init__(self, scale=0.1):
        super(Inception_resnet_B, self).__init__()
        self.scale = scale

        self.branch0 = BasicConv2d(1152, 192, kernel_size=1, stride=1)
        
        self.branch1 = nn.Sequential(
            BasicConv2d(1152, 128, kernel_size=1, stride=1),
            BasicConv2d(128, 160, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(160, 192, kernel_size=(7, 1), stride=1, padding=(3, 0))
        )

        self.conv2d = nn.Conv2d(384, 1152, kernel_size=1, stride=1)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        out = torch.cat((x0, x1), 1)
        out =self.conv2d(out)
        out = x + self.scale * out
        out = self.relu(out)

        return out

class Inception_resnet_C(nn.Module):
    def __init__(self, scale=0.1):
        super(Inception_resnet_C, self).__init__()
        self.scale = scale

        self.branch0 = BasicConv2d(2144, 192, kernel_size=1, stride=1)
        
        self.branch1 = nn.Sequential(
            BasicConv2d(2144, 192, kernel_size=1, stride=1),
            BasicConv2d(192, 224, kernel_size=(1, 3), stride=1, padding=(0, 1)),
            BasicConv2d(224, 256, kernel_size=(3, 1), stride=1, padding=(1, 0))
        )

        self.conv2d = nn.Conv2d(448, 2144, kernel_size=1, stride=1)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        out = torch.cat((x0, x1), 1)
        out =self.conv2d(out)
        out = x + self.scale * out
        out = self.relu(out)

        return out

class Reduction_A(nn.Module):
    def __init__(self):
        super(Reduction_A, self).__init__()
        self.branch0 = nn.MaxPool2d(3, stride=2)

        self.branch1 = BasicConv2d(384, 384, kernel_size=3, stride=2)

        self.branch2 = nn.Sequential(
            BasicConv2d(384, 256, kernel_size=1, stride=1),
            BasicConv2d(256, 256, kernel_size=3, stride=1, padding=1),
            BasicConv2d(256, 384, kernel_size=3, stride=2)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)

        out = torch.cat((x0, x1, x2), 1)

        return out

class Reduction_B(nn.Module):
    def __init__(self):
        super(Reduction_B, self).__init__()
        self.branch0 = nn.MaxPool2d(3, stride=2)

        self.branch1 = nn.Sequential(
            BasicConv2d(1152, 256, kernel_size=1, stride=1),
            BasicConv2d(256, 384, kernel_size=3, stride=2)
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(1152, 256, kernel_size=1, stride=1),
            BasicConv2d(256, 288, kernel_size=3, stride=2)
        )

        self.branch3 = nn.Sequential(
            BasicConv2d(1152, 256, kernel_size=1, stride=1),
            BasicConv2d(256, 288, kernel_size=3, stride=1, padding=1),
            BasicConv2d(288, 320, kernel_size=3, stride=2),
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Inception_ResNetV2(nn.Module):
    def __init__(self, num_classes=10, dropout_prob=0.8):
        super(Inception_ResNetV2, self).__init__()
        self.input_shape = None
        self.input_size = (299, 299, 3)
        self.mean = None
        self.std = None

        self.features = nn.Sequential(
            BasicConv2d(3, 32, kernel_size=3, stride=2),
            BasicConv2d(32, 32, kernel_size=3, stride=1),
            BasicConv2d(32, 64, kernel_size=3, stride=1, padding=1),
            Stem_block_1(),
            Stem_block_2(),
            Stem_block_3(),
            Inception_resnet_A(scale=0.17),
            Inception_resnet_A(scale=0.17),
            Inception_resnet_A(scale=0.17),
            Inception_resnet_A(scale=0.17),
            Inception_resnet_A(scale=0.17),
            Reduction_A(),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Inception_resnet_B(scale=0.1),
            Reduction_B(),
            Inception_resnet_C(scale=0.2),
            Inception_resnet_C(scale=0.2),
            Inception_resnet_C(scale=0.2),
            Inception_resnet_C(scale=0.2),
            Inception_resnet_C(scale=0.2)
        )

        self.avg_pool = nn.AdaptiveAvgPool2d(1)
        self.dropout = nn.Dropout(dropout_prob)
        self.last_linear = nn.Linear(2144, num_classes)

    def forward(self, input):
        x = self.features(input)
        x = self.avg_pool(x)
        x = self.dropout(x)
        x = self.last_linear(x.view(x.size(0), -1))

        return x
