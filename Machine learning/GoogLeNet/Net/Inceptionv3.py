from __future__ import print_function, division, absolute_import
import torch
import torch.nn as nn
import torch.nn.functional as F
import os
import sys

class BasicConv2d(nn.Module):

    def __init__(self, in_planes, out_planes, kernel_size, stride, padding=0):
        super(BasicConv2d, self).__init__()
        self.conv = nn.Conv2d(in_planes, out_planes,
                              kernel_size=kernel_size, stride=stride,
                              padding=padding, bias=False)
        self.bn = nn.BatchNorm2d(out_planes,
                                 eps=0.001,
                                 momentum=0.1,
                                 affine=True)
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x

class Mixed_5b(nn.Module):
    def __init__(self):
        super(Mixed_5b, self).__init__()
        self.branch0 = BasicConv2d(288, 64, kernel_size=1, stride=1)
        
        self.branch1 = nn.Sequential(
            BasicConv2d(288, 48, kernel_size=1, stride=1),
            BasicConv2d(48, 64, kernel_size=5, stride=1, padding=2)
        )
        
        self.branch2 = nn.Sequential(
            BasicConv2d(288, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 96, kernel_size=3, stride=1, padding=1),
            BasicConv2d(96, 96, kernel_size=3, stride=1, padding=1)
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(288, 32, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_5c(nn.Module):
    def __init__(self):
        super(Mixed_5c, self).__init__()
        self.branch0 = BasicConv2d(256, 64, kernel_size=1, stride=1)
        
        self.branch1 = nn.Sequential(
            BasicConv2d(256, 48, kernel_size=1, stride=1),
            BasicConv2d(48, 64, kernel_size=5, stride=1, padding=2)
        )
        
        self.branch2 = nn.Sequential(
            BasicConv2d(256, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 96, kernel_size=3, stride=1, padding=1),
            BasicConv2d(96, 96, kernel_size=3, stride=1, padding=1)
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(256, 64, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_5d(nn.Module):
    def __init__(self):
        super(Mixed_5d, self).__init__()
        self.branch0 = BasicConv2d(288, 64, kernel_size=1, stride=1)
        
        self.branch1 = nn.Sequential(
            BasicConv2d(288, 48, kernel_size=1, stride=1),
            BasicConv2d(48, 64, kernel_size=5, stride=1, padding=2)
        )
        
        self.branch2 = nn.Sequential(
            BasicConv2d(288, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 96, kernel_size=3, stride=1, padding=1),
            BasicConv2d(96, 96, kernel_size=3, stride=1, padding=1)
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(288, 64, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_6a(nn.Module):
    def __init__(self):
        super(Mixed_6a, self).__init__()
        self.branch0 = BasicConv2d(288, 384, kernel_size=3, stride=2)

        self.branch1 = nn.Sequential(
            BasicConv2d(288, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 96, kernel_size=3, stride=1),
            BasicConv2d(96, 96, kernel_size=3, stride=1)
        )

        self.branch2 = nn.MaxPool2d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)

        out = torch.cat((x0, x1, x2), 1)

        return out

class Mixed_6a(nn.Module):
    def __init__(self):
        super(Mixed_6a, self).__init__()
        self.branch0 = BasicConv2d(288, 384, kernel_size=3, stride=2)

        self.branch1 = nn.Sequential(
            BasicConv2d(288, 64, kernel_size=1, stride=1),
            BasicConv2d(64, 96, kernel_size=3, stride=1, padding=1),
            BasicConv2d(96, 96, kernel_size=3, stride=2)
        )

        self.branch2 = nn.MaxPool2d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)

        out = torch.cat((x0, x1, x2), 1)

        return out

class Mixed_6b(nn.Module):
    def __init__(self):
        super(Mixed_6b, self).__init__()
        self.branch0 = BasicConv2d(768, 192, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(768, 128, kernel_size=1, stride=1),
            BasicConv2d(128, 128, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(128, 192, kernel_size=(7, 1), stride=1, padding=(3, 0))
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(768, 128, kernel_size=1, stride=1),
            BasicConv2d(128, 128, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(128, 128, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(128, 128, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(128, 192, kernel_size=(1, 7), stride=1, padding=(0, 3))
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(768, 192, kernel_size=1, stride=1 , padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_6c(nn.Module):
    def __init__(self):
        super(Mixed_6c, self).__init__()
        self.branch0 = BasicConv2d(768, 192, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(768, 160, kernel_size=1, stride=1),
            BasicConv2d(160, 160, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(160, 192, kernel_size=(7, 1), stride=1, padding=(3, 0))
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(768, 160, kernel_size=1, stride=1),
            BasicConv2d(160, 160, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(160, 160, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(160, 160, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(160, 192, kernel_size=(1, 7), stride=1, padding=(0, 3))
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(768, 192, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_6d(nn.Module):
    def __init__(self):
        super(Mixed_6d, self).__init__()
        self.branch0 = BasicConv2d(768, 192, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(768, 160, kernel_size=1, stride=1),
            BasicConv2d(160, 160, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(160, 192, kernel_size=(7, 1), stride=1, padding=(3, 0))
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(768, 160, kernel_size=1, stride=1),
            BasicConv2d(160, 160, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(160, 160, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(160, 160, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(160, 192, kernel_size=(1, 7), stride=1, padding=(0, 3))
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(768, 192, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_6e(nn.Module):
    def __init__(self):
        super(Mixed_6e, self).__init__()
        self.branch0 = BasicConv2d(768, 192, kernel_size=1, stride=1)

        self.branch1 = nn.Sequential(
            BasicConv2d(768, 192, kernel_size=1, stride=1),
            BasicConv2d(192, 192, kernel_size=(1, 7), stride=1, padding=1),
            BasicConv2d(192, 192, kernel_size=(7, 1), stride=1, padding=2)
        )

        self.branch2 = nn.Sequential(
            BasicConv2d(768, 192, kernel_size=1, stride=1),
            BasicConv2d(192, 192, kernel_size=(7, 1), stride=1, padding=2),
            BasicConv2d(192, 192, kernel_size=(1, 7), stride=1, padding=1),
            BasicConv2d(192, 192, kernel_size=(7, 1), stride=1, padding=2),
            BasicConv2d(192, 192, kernel_size=(1, 7), stride=1, padding=1)
        )

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(768, 192, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)
        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_7a(nn.Module):
    def __init__(self):
        super(Mixed_7a, self).__init__()
        self.branch0 = nn.Sequential(
            BasicConv2d(768, 192, kernel_size=1, stride=1),
            BasicConv2d(192, 320, kernel_size=3, stride=2)
        )

        self.branch1 = nn.Sequential(
            BasicConv2d(768, 192, kernel_size=1, stride=1),
            BasicConv2d(192, 192, kernel_size=(1, 7), stride=1, padding=(0, 3)),
            BasicConv2d(192, 192, kernel_size=(7, 1), stride=1, padding=(3, 0)),
            BasicConv2d(192, 192, kernel_size=3, stride=2)
        )

        self.branch2 = nn.MaxPool2d(3, stride=2)

    def forward(self, x):
        x0 = self.branch0(x)
        x1 = self.branch1(x)
        x2 = self.branch2(x)

        out = torch.cat((x0, x1, x2), 1)
        return out

class Mixed_7b(nn.Module):
    def __init__(self):
        super(Mixed_7b, self).__init__()
        self.branch0 = BasicConv2d(1280, 320, kernel_size=1, stride=1)
        
        self.branch1 = BasicConv2d(1280, 384, kernel_size=1, stride=1)
        self.branch1_1a = BasicConv2d(384, 384, kernel_size=(1, 3), stride=1, padding=(0, 1))
        self.branch1_1b = BasicConv2d(384, 384, kernel_size=(3, 1), stride=1, padding=(1, 0))
        
        self.branch2 = nn.Sequential(
            BasicConv2d(1280, 448, kernel_size=1, stride=1),
            BasicConv2d(448, 384, kernel_size=3, stride=1),  
        )
        self.branch2_1a = BasicConv2d(384, 384, kernel_size=(1, 3), stride=1, padding=(0, 1))
        self.branch2_1b = BasicConv2d(384, 384, kernel_size=(3, 1), stride=1, padding=(1, 0))

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(1280, 192, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)

        x1 = self.branch1(x)
        x1 = [
            self.branch1_1a(x1),
            self.branch1_1b(x1)
        ]
        x1 = torch.cat(x1, 1)

        x2 = self.branch1(x)
        x2 = [
            self.branch2_1a(x2),
            self.branch2_1b(x2)
        ]
        x2 = torch.cat(x2, 1)

        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class Mixed_7c(nn.Module):
    def __init__(self):
        super(Mixed_7c, self).__init__()
        self.branch0 = BasicConv2d(2048, 320, kernel_size=1, stride=1)
        
        self.branch1 = BasicConv2d(2048, 384, kernel_size=1, stride=1)
        self.branch1_1a = BasicConv2d(384, 384, kernel_size=(1, 3), stride=1, padding=(0, 1))
        self.branch1_1b = BasicConv2d(384, 384, kernel_size=(3, 1), stride=1, padding=(1, 0))
        
        self.branch2 = nn.Sequential(
            BasicConv2d(2048, 448, kernel_size=1, stride=1),
            BasicConv2d(448, 384, kernel_size=3, stride=1),  
        )
        self.branch2_1a = BasicConv2d(384, 384, kernel_size=(1, 3), stride=1, padding=(0, 1))
        self.branch2_1b = BasicConv2d(384, 384, kernel_size=(3, 1), stride=1, padding=(1, 0))

        self.branch3 = nn.Sequential(
            nn.AvgPool2d(3, stride=1),
            BasicConv2d(2048, 192, kernel_size=1, stride=1, padding=1)
        )

    def forward(self, x):
        x0 = self.branch0(x)

        x1 = self.branch1(x)
        x1 = [
            self.branch1_1a(x1),
            self.branch1_1b(x1)
        ]
        x1 = torch.cat(x1, 1)

        x2 = self.branch1(x)
        x2 = [
            self.branch2_1a(x2),
            self.branch2_1b(x2)
        ]
        x2 = torch.cat(x2, 1)

        x3 = self.branch3(x)

        out = torch.cat((x0, x1, x2, x3), 1)

        return out

class InceptionV3(nn.Module):
    def __init__(self, num_classes=2):
        super(InceptionV3, self).__init__()
        # Special attributes
        self.input_shape = None
        self.input_size = (299, 299, 3)
        self.mean = None
        self.std = None
        # Modules
        self.features = nn.Sequential(
            BasicConv2d(3, 32, kernel_size=3, stride=2),
            BasicConv2d(32, 32, kernel_size=3, stride=1),
            BasicConv2d(32, 64, kernel_size=3, stride=1, padding=1),
            nn.MaxPool2d(3, stride=2),
            BasicConv2d(64, 80, kernel_size=3, stride=1),
            BasicConv2d(80, 192, kernel_size=3, stride=2),
            BasicConv2d(192, 288, kernel_size=3, stride=1, padding=1),
            Mixed_5b(),
            Mixed_5c(),
            Mixed_5d(),
            Mixed_6a(),
            Mixed_6b(),
            Mixed_6c(),
            Mixed_6d(),
            Mixed_6e(),
            Mixed_7a(),
            Mixed_7b(),
            Mixed_7c()
        )
        
        self.last_linear = nn.Linear(2048, num_classes)

    def logits(self, features):
        adaptiveAvgPoolWidth = features.shape[2]
        x = F.avg_pool2d(features, kernel_size=adaptiveAvgPoolWidth)
        x = x.view(x.size(0), -1)
        x = self.last_linear(x)
        return x

    def forward(self, input):
        x = self.features(input)
        x = self.logits(x)
        return x